import type Product from "@/types/Product";
import http from "./axios";
function getProducts() {
  return http.get("/products");
}

function addNewProduct(product: Product) {
  return http.post("/products", product);
}

function updateProduct(id: number, product: Product) {
  return http.patch("/products/" + id, product);
}

function deleteProduct(id: number) {
  return http.delete("/products/" + id);
}

export default { getProducts, addNewProduct, updateProduct, deleteProduct };
