import axios from "axios";

const instance = axios.create({
  baseURL: "http://localhost:3000",
});

function delay(time: number) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve(true);
    }, time);
  });
}

// Add a request interceptor
instance.interceptors.request.use(
  async function (config) {
    // Do something before request is sent
    // await delay(1000);
    return config;
  },
  function (error) {
    // Do something with request error
    return Promise.reject(error);
  }
);

export default instance;
